package com.aliasgarlabs.oneplus2fanapp;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Launch extends Activity {

	
	Button b1,b2,b3,b4,b5;
	TextView t;
	String left,right;
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch);
        b1=(Button) findViewById(R.id.button1);
        b2=(Button) findViewById(R.id.button2);
        b3=(Button) findViewById(R.id.button3);
        b4=(Button) findViewById(R.id.button4);
        b5=(Button) findViewById(R.id.button5);
        Bundle bundle = getIntent().getExtras();
       try{ left = bundle.getString("left");
         right=bundle.getString("right");}catch(Exception e){}
       // t=(TextView)findViewById(R.id.lune);
        //t.setText(left+" "+right);
        b3.setOnClickListener(new View.OnClickListener() 
        {
			
			@Override
			public void onClick(View arg0) 
			{
				// TODO Auto-generated method stub
				Intent i;
		    	i=new Intent(Launch.this,Calibrate.class);
		    	startActivity(i);
				
				
			}
		});
        b2.setOnClickListener(new View.OnClickListener() 
        {
			
			@Override
			public void onClick(View arg0) 
			{
				// TODO Auto-generated method stub
				Intent i;
		    	i=new Intent(Launch.this,FileSelect.class);
		    	startActivity(i);
				
				
			}
		});
        b1.setOnClickListener(new View.OnClickListener() 
        {
			
			@Override
			public void onClick(View arg0) 
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent( Launch.this,Play.class);
				intent.putExtra("left", left);
				intent.putExtra("right", right);
				startActivity(intent);
				
			}
		});
        
        b5.setOnClickListener(new View.OnClickListener() 
        {
			
			@Override
			public void onClick(View arg0) 
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent( Launch.this,About.class);
			
				startActivity(intent);
				
			}
		});
        b4.setOnClickListener(new View.OnClickListener() 
        {
			
			@Override
			public void onClick(View arg0) 
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent( Launch.this, HowToUse.class);
			
				startActivity(intent);
				
			}
		});
    
    }

 
    
}
