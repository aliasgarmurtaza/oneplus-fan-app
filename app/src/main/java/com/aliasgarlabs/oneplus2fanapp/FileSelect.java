package com.aliasgarlabs.oneplus2fanapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FileSelect extends Activity {

	EditText l,r;
	Button b;
	public String l1;
	public String r1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_select);
		l=(EditText) findViewById(R.id.editText1);
		r=(EditText) findViewById(R.id.editText2);
		b=(Button) findViewById(R.id.bu);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) 
			{
				// TODO Auto-generated method stub
				
				try{
					 l1= l.getText().toString(); 
					 r1=r.getText().toString();
					}catch(Exception e){Toast.makeText(FileSelect.this, "ERROR", Toast.LENGTH_SHORT).show();}
				Intent intent = new Intent(FileSelect.this, MainActivity.class);
				intent.putExtra("left", l1);
				intent.putExtra("right", r1);
				startActivity(intent);
			
			}
		});
	}

	

}
