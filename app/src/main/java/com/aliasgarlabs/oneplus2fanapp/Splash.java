package com.aliasgarlabs.oneplus2fanapp;

import android.app.Activity;
        import android.content.Intent;
        import android.os.Bundle;
        import android.os.Handler;
import android.widget.TextView;

import java.util.Random;

public class Splash extends Activity {

    private static int SPLASH_TIME_OUT = 4000;
    private TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final String names[] = {"MAGNIFICENT",
                "MAJESTIC",
                "AWE-INSPIRING",
                "BREATHTAKING",
                "MONUMENTAL",
                "ELEGANT",
                "BEAUTIFUL",
                "GORGEOUS",
                "SPLENDACIOUS",    };

        label = (TextView) findViewById(R.id.label);
        final Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                label.setText(names[randInt(0, 8)]);
                handler.postDelayed(this, 650);
            }
        };

        handler.postDelayed(r, 0);


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(Splash.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}