package com.aliasgarlabs.oneplus2fanapp;

import android.net.Uri;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class Play extends Activity {
	VideoView mVideoView;
	
	VideoView mVideoView1;
	String left,right;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);
		Bundle bundle = getIntent().getExtras();
		try{ left = bundle.getString("left");
        right=bundle.getString("right");}catch(Exception e){}
		
		
		
		View decorView = getWindow().getDecorView();
	     // Hide the status bar.
	     int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
	     decorView.setSystemUiVisibility(uiOptions);
	     // Remember that you should never show the action bar if the
	     // status bar is hidden, so hide that too if necessary.
	     ActionBar actionBar = getActionBar();
	     actionBar.hide();
	   // l=Uri.parse(left);
	   // r=Uri.parse(right);
	     View vvv = getWindow().getDecorView();
	  //Hide both the navigation bar and the status bar.
	  // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
	  // a general rule, you should design your app to hide the status bar whenever you
	  // hide the navigation bar.
	     int uuu = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
	     vvv.setSystemUiVisibility(uuu);
	     
	     
	     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	      mVideoView  = (VideoView)findViewById(R.id.lu);
	      mVideoView1=  (VideoView)findViewById(R.id.ru);
	    MediaController a = new MediaController(this);
	     MediaController b = new MediaController(this);   
	   mVideoView1.setMediaController(a);
	   try{   mVideoView1.setVideoPath(left);
	     mVideoView.setMediaController(b);
	      mVideoView.setVideoPath(right);}catch(Exception e){
	    	  AlertDialog.Builder ba;
			 	ba=new AlertDialog.Builder(Play.this);
			 	ba.setMessage("GO BACK AND SELECT A FILE !").setTitle("Set path").
			 	setPositiveButton("OK", new DialogInterface.OnClickListener(){

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						
						 
					}});
			 	ba.create().show();
	      }
	   
	      Thread t= new Thread(new Runnable() 
	      { 
	           public void run()
	           {        
	         	
	         	  mVideoView.start();
	         	  mVideoView1.start(); 
	         	  
	         	  
	           }
	       });
	      t.start();
	        	  
	        	  
	     
	
	
	}

	
}
